import Head from 'next/head'
import { Inter } from '@next/font/google'
import s from '@/styles/Home.module.css'
import fetchStories from '@/lib/fetch-stories'
import cn from 'clsx'
import { InferGetStaticPropsType } from 'next'
import Listing from '@/components/Listing'
import sortBy from '@/lib/sort-by'
import { sortOrders } from '@/components/Sorter'

const inter = Inter({ subsets: ['latin'] })
export async function getStaticProps() {
  const stories = await fetchStories()
  return {
    props: { stories: stories.sort(sortBy(sortOrders[0].prop)) },
    revalidate: 60,
  }
}

export default function Home({ stories }: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <>
      <Head>
        <title>Project Stories</title>
        <meta name="description" content="Project stories" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={cn(s.root, inter.className)}>
        <Listing stories={stories} />
      </main>
    </>
  )
}

import * as process from 'process'

const fetchStories = async () => {
  if (!process.env.ENDPOINT) {
    console.error('Missing ENDPOINT in .env.local file!')
    return []
  }
  try {
    const controller = new AbortController()
    const timeout = setTimeout(() => {
      return controller.abort()
    }, 5000)
    const res = await fetch(process.env.ENDPOINT, {
      signal: controller.signal,
    })
    clearTimeout(timeout)
    return await res.json()
  } catch (e) {
    console.error('Error fetching remote data')
  }
  return []
}

export default fetchStories

const sortBy = <T>(key: keyof T, desc: boolean = false): ((a: T, b: T) => 1 | 0 | -1) => {
  if (desc) {
    return (a, b) => (a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0)
  }
  return (a, b) => (a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0)
}

export default sortBy

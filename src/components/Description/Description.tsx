import { FC } from 'react'
import ReactMarkdown from 'react-markdown'
import s from './Description.module.css'
const Description: FC<{ descripton: string }> = ({ descripton }) => {
  return (
    <div className={s.root}>
      <ReactMarkdown className={s.md}>{descripton}</ReactMarkdown>
    </div>
  )
}

export default Description

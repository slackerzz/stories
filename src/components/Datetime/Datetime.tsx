import { FC } from 'react'

interface DatetimeProps {
  datetimeString: string
}

const Datetime: FC<DatetimeProps> = ({ datetimeString }) => {
  const date = new Date(datetimeString)
  if (!date.valueOf()) {
    console.error('Not a valid date object')
    return <></>
  }
  const timestring = date.toDateString() + ' ' + date.toLocaleTimeString()
  return <span>{timestring}</span>
}

export default Datetime

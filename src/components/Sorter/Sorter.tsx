import { FC, Fragment } from 'react'
import s from './Sorter.module.css'
import { Listbox, Transition } from '@headlessui/react'
import { StoryInterface } from '@/components/Story'
import { ArrowDownIcon, ArrowUpIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid'
import cn from 'clsx'

export interface SortOrderInterface {
  id: number
  label: string
  prop: keyof StoryInterface
  desc: boolean
}

export const sortOrders: SortOrderInterface[] = [
  { id: 1, label: 'Name', prop: 'name', desc: false },
  { id: 2, label: 'Name', prop: 'name', desc: true },
  { id: 3, label: 'Created at', prop: 'created_at', desc: false },
  { id: 4, label: 'Created at', prop: 'created_at', desc: true },
]

interface SorterProps {
  selectedSortOrder: SortOrderInterface
  setSelecterSortOrder: (order: SortOrderInterface) => void
}
const Sorter: FC<SorterProps> = ({ selectedSortOrder, setSelecterSortOrder }) => {
  return (
    <Listbox value={selectedSortOrder} onChange={setSelecterSortOrder}>
      <div className={s.root}>
        <span className={s.label}>Sort by</span>
        <div className={s.buttonWrapper}>
          <Listbox.Button className={s.listboxButton}>
            <span className={s.buttonLabel}>
              {selectedSortOrder.label}{' '}
              {!selectedSortOrder.desc ? <ArrowDownIcon className={s.icon} /> : <ArrowUpIcon className={s.icon} />}
            </span>
            <span className={s.selectIconContainer}>
              <ChevronUpDownIcon className={s.selectIcon} aria-hidden="true" />
            </span>
          </Listbox.Button>
          <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
            <Listbox.Options className={s.options}>
              {sortOrders.map((sortOrder) => (
                <Listbox.Option
                  key={sortOrder.id}
                  value={sortOrder}
                  className={({ active }) =>
                    cn(s.option, {
                      [s.active]: active,
                    })
                  }
                >
                  {sortOrder.label}{' '}
                  {!sortOrder.desc ? <ArrowDownIcon className={s.icon} /> : <ArrowUpIcon className={s.icon} />}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </div>
    </Listbox>
  )
}

export default Sorter

import { FC } from 'react'
import s from './StoryDetails.module.css'
import Datetime from '@/components/Datetime'
import { StoryInterface } from '@/components/Story'
const StoryDetails: FC<{ story: StoryInterface }> = ({ story }) => {
  return (
    <div className={s.root}>
      <div className={s.row}>
        <span>Kind</span>
        <span>{story.kind}</span>
      </div>
      <div className={s.row}>
        <span>State</span>
        <span>{story.current_state}</span>
      </div>
      <div className={s.row}>
        <span>Project ID</span>
        <span>{story.project_id}</span>
      </div>
      <div className={s.row}>
        <span>Type</span>
        <span>{story.story_type}</span>
      </div>
      <div className={s.row}>
        <span>Estimate</span>
        {story.estimate && (
          <span>
            {story.estimate} {story.estimate === 1 ? 'point' : 'points'}
          </span>
        )}
        {!story.estimate && <span> - </span>}
      </div>
      <div className={s.row}>
        <span>Priority</span>
        <span>{story.story_priority}</span>
      </div>
      <div className={s.row}>
        <span>Requested by</span>
        <span>{story.requested_by_id}</span>
      </div>
      <div className={s.row}>
        <span>Own by</span>
        <span>{story.owner_ids.join(',')}</span>
      </div>
      <div className={s.row}>
        <span>Created at</span>
        <Datetime datetimeString={story.created_at} />
      </div>
      <div className={s.row}>
        <span>Updated at</span>
        <Datetime datetimeString={story.updated_at} />
      </div>
      <div className={s.row}>
        <span>Accepted at</span>
        <Datetime datetimeString={story.accepted_at} />
      </div>
    </div>
  )
}

export default StoryDetails

import { FC } from 'react'
import { Disclosure } from '@headlessui/react'
import { ArrowTopRightOnSquareIcon, ChevronUpIcon, HashtagIcon, UserIcon, UsersIcon } from '@heroicons/react/20/solid'
import Link from 'next/link'
import cn from 'clsx'
import s from './Story.module.css'
import StoryDetails from '@/components/StoryDetails'
import Label from '@/components/Label'
import Badge from '@/components/Badge'
import ReactMarkdown from 'react-markdown'
import Description from '@/components/Description'

export type StoryType = 'feature' | 'bug' | 'chore' | 'release'

export type StoryState =
  | 'accepted'
  | 'delivered'
  | 'finished'
  | 'started'
  | 'rejected'
  | 'planned'
  | 'unstarted'
  | 'unscheduled'

export type StoryPriority = 'p0' | 'p1' | 'p2' | 'p3' | 'p4'
export type StoryLabel = {
  id: number
  project_id: number
  kind: string
  name: string
  created_at: string
  updated_at: string
}
export interface StoryInterface {
  id: number
  project_id: number
  name: string
  description: string
  story_type: StoryType
  current_state: StoryState
  estimate?: number
  accepted_at: string
  requested_by_id: number
  owned_by_id: number
  owner_ids: number[]
  created_at: string
  updated_at: string
  url: string
  kind: string
  story_priority: StoryPriority
  labels: StoryLabel[]
}

const Story: FC<{ story: StoryInterface }> = ({ story }) => {
  return (
    <Disclosure as="li" className={s.root} defaultOpen={false}>
      {({ open }) => (
        <>
          <Disclosure.Button className={cn(s.button, { [s.opened]: open })}>
            <div className={s.wrapper}>
              <Badge variant="type" value={story.story_type} />
              <Badge variant="priority" value={story.story_priority} />
              <Badge variant="state" value={story.current_state} />
              <ReactMarkdown className={s.name}>{story.name}</ReactMarkdown>
            </div>
            <ChevronUpIcon
              className={cn('min-w-[20px] h-5 w-5 text-blue-500', {
                ['rotate-180']: !open,
              })}
            />
          </Disclosure.Button>
          <Disclosure.Panel className={s.panel}>
            <div className={s.infoGroup}>
              <HashtagIcon className={s.icon} />
              {story.id}
              <Link href={story.url} target="_blank">
                <ArrowTopRightOnSquareIcon className={s.icon} />
              </Link>
            </div>
            {story.description && <Description descripton={story.description} />}
            {story.labels.length > 0 && (
              <div className={s.labels}>
                {story.labels.map((label) => (
                  <Label label={label} key={label.id} />
                ))}
              </div>
            )}
            <StoryDetails story={story} />
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  )
}

export default Story

import { FC } from 'react'
import s from './Label.module.css'
import { StoryLabel } from '@/components/Story'

interface LabelProps {
  label: StoryLabel
}
const Label: FC<LabelProps> = ({ label }) => {
  return (
    <span className={s.root}>
      <svg className={s.icon} fill="currentColor" viewBox="0 0 8 8">
        <circle cx={4} cy={4} r={3} />
      </svg>
      {label.name}
    </span>
  )
}

export default Label

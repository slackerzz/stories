import { FC, FormEvent, useEffect, useState } from 'react'
import s from './Listing.module.css'
import Story, { StoryInterface } from '@/components/Story'
import Image from 'next/image'
import Sorter, { SortOrderInterface, sortOrders } from '@/components/Sorter'
import sortBy from '@/lib/sort-by'
import SearchBox from '@/components/SearchBox'

interface ListingProps {
  stories: StoryInterface[]
}
const Listing: FC<ListingProps> = ({ stories }) => {
  const [sortedStories, setSortedStories] = useState<StoryInterface[]>(stories)
  const [selectedSortOrder, setSelectedSortOrder] = useState<SortOrderInterface>(sortOrders[0])
  const [searchTerm, setSearchTerm] = useState<string>('')

  const handleSortOrderChange = (order: SortOrderInterface) => {
    setSelectedSortOrder(order)
  }

  const handleSearchTermChange = (e: FormEvent<HTMLInputElement>) => {
    setSearchTerm(e.currentTarget.value)
  }

  useEffect(() => {
    setSortedStories(
      [...stories]
        .filter((story: StoryInterface) => story.name.toLowerCase().includes(searchTerm.toLowerCase()))
        .sort(sortBy(selectedSortOrder.prop, selectedSortOrder.desc))
    )
  }, [stories, selectedSortOrder, searchTerm])

  const emptyState = (
    <div className={s.empty}>
      <h3 className={s.heading}>No stories found</h3>
      <p className={s.text}>Try another search term</p>
      <Image className={s.image} src="/confused.gif" alt="404" width="490" height="476" />
    </div>
  )

  const list = (
    <ul className={s.listRoot}>
      {sortedStories.map((story: StoryInterface) => {
        return <Story story={story} key={story.id} />
      })}
    </ul>
  )
  return (
    <div className={s.root}>
      <h1 role="heading" className={s.heading}>
        Project stories
      </h1>
      <div className={s.container}>
        <div className={s.actions}>
          <SearchBox searchTerm={searchTerm} handleChange={handleSearchTermChange} />
          <Sorter selectedSortOrder={selectedSortOrder} setSelecterSortOrder={handleSortOrderChange} />
        </div>
        {sortedStories.length === 0 ? emptyState : list}
      </div>
    </div>
  )
}

export default Listing

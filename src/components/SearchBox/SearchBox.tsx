import { FC, FormEvent } from 'react'
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid'
import s from './SearchBox.module.css'
interface SearchBoxProps {
  searchTerm: string
  handleChange: (e: FormEvent<HTMLInputElement>) => void
}
const SearchBox: FC<SearchBoxProps> = ({ searchTerm, handleChange }) => {
  return (
    <div className={s.root}>
      <div className={s.iconWrapper}>
        <MagnifyingGlassIcon className={s.icon} aria-hidden="true" />
      </div>
      <input
        type="search"
        name="search"
        id="search"
        className={s.input}
        placeholder="Search by name"
        autoComplete="off"
        onChange={handleChange}
      />
    </div>
  )
}

export default SearchBox

import { FC } from 'react'
import { StoryPriority, StoryState, StoryType } from '@/components/Story'
import cn from 'clsx'
import s from './Badge.module.css'
import { BugAntIcon, CogIcon, FlagIcon, LightBulbIcon } from '@heroicons/react/20/solid'

interface BadgeProps {
  variant: 'state' | 'type' | 'priority'
  value: StoryType | StoryState | StoryPriority
  className?: string
}
const Badge: FC<BadgeProps> = ({ variant, value, className = '' }) => {
  const rootClassName = cn(
    s.root,
    {
      [s.priority]: variant === 'priority',
      [s.state]: variant === 'state',
      [s.type]: variant === 'type',
      [s.green]: ['accepted', 'delivered', 'finished'].includes(value),
      [s.yellow]: ['started', 'planned'].includes(value),
      [s.gray]: ['unstarted', 'unscheduled', 'finished'].includes(value),
      [s.red]: value === 'rejected',
      [s.p0]: value === 'p0',
      [s.p1]: value === 'p1',
      [s.p2]: value === 'p2',
      [s.p3]: value === 'p3',
      [s.p4]: value === 'p4',
      [s.bug]: value === 'bug',
      [s.chore]: value === 'chore',
      [s.feature]: value === 'feature',
      [s.release]: value === 'release',
    },
    className
  )
  return (
    <span className={rootClassName} title={value}>
      {value === 'bug' && <BugAntIcon className={s.icon} />}
      {value === 'chore' && <CogIcon className={s.icon} />}
      {value === 'feature' && <LightBulbIcon className={s.icon} />}
      {value === 'release' && <FlagIcon className={s.icon} />}
      <span className={s.text}>{value}</span>
    </span>
  )
}

export default Badge

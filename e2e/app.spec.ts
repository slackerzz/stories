import { test, expect } from '@playwright/test'

test('should load without errors', async ({ page }) => {
  await page.goto('http://localhost:3000')
  await expect(page.locator('h1')).toContainText('Project stories')
})

test('content is visible when opening a list element', async ({ page }) => {
  await page.goto('http://localhost:3000/')
  await page.locator('ul[class^=Listing_listRoot] > li:first-child button').click()
  await expect(page.getByText('Kind')).toBeVisible()
})

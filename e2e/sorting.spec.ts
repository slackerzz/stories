import { test, expect } from '@playwright/test'

test('test', async ({ page }) => {
  await page.goto('http://localhost:3000/')
  const firstElementPreSort = await page.locator('ul[class^=Listing_listRoot] > li:first-child div[class^=Story_name]')
  const lastElementPreSort = await page.locator('ul[class^=Listing_listRoot] > li:last-child div[class^=Story_name]')
  // save the text content of the first and last element of the list
  const firstElementPreSortingText = await firstElementPreSort.textContent()
  const lastElementPreSortingText = await lastElementPreSort.textContent()

  // sort by name descending
  await page.getByRole('button', { name: 'Name' }).click()
  await page.locator('ul[class^=Sorter_options] li:nth-child(2)').click()

  const firstElement = await page.locator('ul[class^=Listing_listRoot] > li:first-child div[class^=Story_name]')
  const lastElement = await page.locator('ul[class^=Listing_listRoot] > li:last-child div[class^=Story_name]')
  await expect(firstElement).toHaveText(lastElementPreSortingText as string)
  await expect(lastElement).toHaveText(firstElementPreSortingText as string)
})

import { test, expect } from '@playwright/test'

test('test', async ({ page }) => {
  await page.goto('http://localhost:3000/')
  await page.getByPlaceholder('Search by name').click()
  await page.getByPlaceholder('Search by name').fill('123')
  await expect(page.locator('h3')).toContainText('No stories found')
})

# Getting Started

## Install dependecies:

```bash
pnpm i
```

## Copy the .env file
```bash
cp .env.template .env.local
```

## Run the development server
```bash
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.